package com.atguigu.gmall0218.payment.PayStrategy;

import org.springframework.stereotype.Component;

/**
 * <pre>
 *    @author  : liquid
 *    email   : xiaokui.li@guigu.com
 *    time    : 2019-12-0416:53
 *    desc    : 输入描述
 *    version : v1.0
 * </pre>
 */
@Component
public class WxPayStrategy implements IPayStrategy {
    /**
     * 微信扫码支付地址
     */
    private static final String WEIXIN_WEB_CODE_ACTION = "/weixin/wxCode";
    @Override
    public String getPayActionUrl() {
        return WEIXIN_WEB_CODE_ACTION;
    }
}
