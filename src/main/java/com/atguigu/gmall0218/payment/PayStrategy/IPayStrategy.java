package com.atguigu.gmall0218.payment.PayStrategy;

/**
 * <pre>
 *    @author  : liquid
 *    email   : xiaokui.li@guigu.com
 *    time    : 2019-12-0416:52
 *    desc    : 支付策略接口
 *    version : v1.0
 * </pre>
 */
public interface IPayStrategy {
    /**
     * 获取支付响应地址
     * @return 支付地址
     */
    String getPayActionUrl();
}
