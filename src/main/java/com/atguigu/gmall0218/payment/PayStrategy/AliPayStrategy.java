package com.atguigu.gmall0218.payment.PayStrategy;

import org.springframework.stereotype.Component;

/**
 * <pre>
 *    @author  : liquid
 *    email   : xiaokui.li@guigu.com
 *    time    : 2019-12-0416:53
 *    desc    : 输入描述
 *    version : v1.0
 * </pre>
 */
@Component
public class AliPayStrategy implements IPayStrategy {
    /**
     * 支付宝WEB即时到账交易响应地址
     */
    private static final String ZFB_WEB_PAY_ACTION = "/alipay/webPayment/alipayapi";
    @Override
    public String getPayActionUrl() {
        return ZFB_WEB_PAY_ACTION;
    }
}
