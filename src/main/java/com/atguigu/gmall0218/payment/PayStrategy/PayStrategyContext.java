package com.atguigu.gmall0218.payment.PayStrategy;

import com.atguigu.gmall0218.payment.exception.BusinessException;
import com.atguigu.gmall0218.payment.util.SpringContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * <pre>
 *    @author  : liquid
 *    email   : xiaokui.li@guigu.com
 *    time    : 2019-12-0416:57
 *    desc    : 输入描述
 *    version : v1.0
 * </pre>
 */
@Component
@Slf4j
public class PayStrategyContext {
    /**
     * 获取支付跳转地址
     * @param payOrgName 支付机构
     * @return String 支付地址
     */
    public static String getPayAction(String payOrgName) {
        IPayStrategy payStrategy = PayStrategyContext.getInstance(payOrgName);
        return payStrategy.getPayActionUrl();
    }

    /**
     * 获取支付配置管理类实例
     * @param payOrgName 支付机构
     * @return IPayStrategy 获取指定的支付配置管理器//alipayConfig
     */
    public static IPayStrategy getInstance(String payOrgName) {
        IPayStrategy payStrategy = (IPayStrategy) SpringContextUtil.getBean(payOrgName);
        if (Objects.nonNull(payStrategy)){
            return payStrategy;
        } else {
            String message = "获取不到该支付类型的： " + payOrgName + "配置管理类实例，请查看配置或暂不支持此支付方式";
            log.error(message);
            throw new BusinessException("0001",message);
        }
    }
}
