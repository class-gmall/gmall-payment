package com.atguigu.gmall0218.payment.mapper;

import com.atguigu.gmall0218.bean.PaymentInfoException;
import tk.mybatis.mapper.common.Mapper;

public interface PaymentInfoExceptionMapper extends Mapper<PaymentInfoException> {
}
