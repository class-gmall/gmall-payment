package com.atguigu.gmall0218.payment.mapper;

import com.atguigu.gmall0218.bean.PaymentInfoLog;
import tk.mybatis.mapper.common.Mapper;

public interface PaymentInfoLogMapper extends Mapper<PaymentInfoLog> {
}
