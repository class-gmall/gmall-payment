package com.atguigu.gmall0218.payment.constant;

/**
 * <pre>
 *    @author  : liquid
 *    email   : xiaokui.li@guigu.com
 *    desc    : 异常编码asasas
 *    version : v1.0
 * </pre>
 */
public class ExceptCodeConstants {
    /**
     * 重复支付
     */
    public static final String REPEATABLE_TRADE = "0001";
    /**
     * 支付策略未设置
     */
    public static final String NO_PAY_STRATEGY = "0002";
    /**
     * 未找到支付方式
     */
    public static final String NO_PAY_STYLE = "0003";
    /**
     * 微信支付失败
     */
    public static final String WX_PAY_FAIL = "0004";
}
