package com.atguigu.gmall0218.payment.service.impl;

import com.atguigu.gmall0218.bean.PaymentInfoLog;
import com.atguigu.gmall0218.payment.mapper.PaymentInfoLogMapper;
import com.atguigu.gmall0218.service.PaymentLogSerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <pre>
 *    @author  : liquid
 *    email   : xiaokui.li@guigu.com
 *    desc    : 支付日志实现类
 *    version : v1.0
 * </pre>
 */
@Service
public class PaymentLogServiceImpl implements PaymentLogSerivce {
    @Autowired
    private PaymentInfoLogMapper paymentInfoLogMapper;
    @Override
    public void savePaymentInfoLog(PaymentInfoLog paymentInfoLog) {
        paymentInfoLogMapper.insert(paymentInfoLog);
    }
}
