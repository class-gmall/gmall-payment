package com.atguigu.gmall0218.payment.service.impl;

import com.atguigu.gmall0218.bean.PaymentInfoException;
import com.atguigu.gmall0218.payment.mapper.PaymentInfoExceptionMapper;
import com.atguigu.gmall0218.service.PaymentExceptionSerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <pre>
 *    @author  : liquid
 *    email   : xiaokui.li@guigu.com
 *    desc    : 异常支付实现类
 *    version : v1.0
 * </pre>
 */
@Service
public class PaymentExceptionServiceImpl implements PaymentExceptionSerivce {
    @Autowired
    private PaymentInfoExceptionMapper paymentInfoExceptionMapper;

    @Override
    public void savePaymentInfoException(PaymentInfoException paymentInfoException) {
        paymentInfoExceptionMapper.insert(paymentInfoException);
    }
}
