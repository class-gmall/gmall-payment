package com.atguigu.gmall0218.payment.service.impl;

import com.atguigu.gmall0218.bean.PaymentInfo;
import com.atguigu.gmall0218.bean.PaymentInfoLog;
import com.atguigu.gmall0218.bean.enums.PaymentStatus;
import com.atguigu.gmall0218.payment.constant.ExceptCodeConstants;
import com.atguigu.gmall0218.payment.exception.BusinessException;
import com.atguigu.gmall0218.service.IPayCenterCombSV;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Objects;


/**
 * 支付平台流水服务业务实现类
 *
 * @author lxk
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class PayCenterCombSVImpl implements IPayCenterCombSV {
    @Autowired
    private PaymentLogServiceImpl paymentLogService;
    @Autowired
    private PaymentServiceImpl paymentSerivce;
    @Override
    public void createPaymentInfo(String orderId ,String payOrgName) {
        //根据订单id查询订单信息,如果不存在，则返回信息，如果存在，然后封装支付参数
        // OrderInfo orderInfo =  orderService.getOrderInfo(orderId);
        // 根据订单信息查询支付信息
        PaymentInfo paymentInfo = new PaymentInfo();
        paymentInfo.setOrderId(orderId);
        //查询支付信息表
        PaymentInfo paymentInfoOld = paymentSerivce.getPaymentInfo(paymentInfo);
        if (Objects.nonNull(paymentInfoOld)){
            //校验此订单是否可以支付
            this.checkOrderCouldPay(paymentInfoOld);
            //TODO 可以直接拿到查询到的信息进行填充
            paymentInfo.setOutTradeNo(orderId);
            paymentInfo.setTotalAmount(12L);
            paymentInfo.setSubject("给老板买xxx1！");
            paymentInfo.setPayOrgId(payOrgName);
            paymentInfo.setPaymentStatus(PaymentStatus.UNPAID);
            modifyPaymentInfo(paymentInfo);
        }else {
            //否则直接插入支付信息表、插入支付信息日志表
            paymentInfo.setOutTradeNo(orderId);
            paymentInfo.setTotalAmount(12L);
            paymentInfo.setSubject("给老板买xxx！");
            paymentInfo.setPaymentStatus(PaymentStatus.UNPAID);
            paymentInfo.setPayOrgId(payOrgName);
            paymentSerivce.savePaymentInfo(paymentInfo);
            PaymentInfoLog paymentInfoLog = new PaymentInfoLog();
            BeanUtils.copyProperties(paymentInfo,paymentInfoLog);
            paymentLogService.savePaymentInfoLog(paymentInfoLog);

        }
    }

    @Override
    public void updatePayCenter(String orderId,String transaction_id) {
        //更新支付信息表
        PaymentInfo paymentInfo = new PaymentInfo();
        paymentInfo.setOrderId(orderId);
        //查询支付信息表
        PaymentInfo paymentInfoUPD = paymentSerivce.getPaymentInfo(paymentInfo);
        paymentInfoUPD.setPaymentStatus(PaymentStatus.PAID);
        paymentInfoUPD.setCallbackTime(new Date());
        paymentInfoUPD.setPayTradeNo(transaction_id);
        paymentSerivce.updatePaymentInfo(orderId,paymentInfoUPD);
        //更新支付日志表
        PaymentInfoLog paymentInfoLog = new PaymentInfoLog();
        paymentInfoLog.setPayOrgId(paymentInfoUPD.getPayOrgId());
        paymentInfoLog.setOutTradeNo(paymentInfoUPD.getOutTradeNo());
        paymentInfoLog.setPayTradeNo(paymentInfoUPD.getPayTradeNo());
        paymentInfoLog.setPaymentStatus(paymentInfoUPD.getPaymentStatus());
        paymentInfoLog.setSubject(paymentInfoUPD.getSubject());
        paymentInfoLog.setOrderId(paymentInfoUPD.getOrderId());
        paymentInfoLog.setTotalAmount(paymentInfoUPD.getTotalAmount());
        paymentLogService.savePaymentInfoLog(paymentInfoLog);
    }

    /**
     * 检查订单是否可以支付
     * @param paymentInfo 支付信息
     */
    private void checkOrderCouldPay(PaymentInfo paymentInfo) {
        // 防止重复支付

        if (PaymentStatus.PAID.name().equals(paymentInfo.getPaymentStatus().name())) {
            throw new BusinessException(ExceptCodeConstants.REPEATABLE_TRADE, "repeat order pay！");
        }
        if (PaymentStatus.ClOSED.name().equals(paymentInfo.getPaymentStatus().name())) {
            throw new BusinessException(ExceptCodeConstants.REPEATABLE_TRADE, "repeat order pay");
        }
    }

    /**
     * 修改支付信息
     * @param paymentInfo 支付信息
     */
    private void modifyPaymentInfo(PaymentInfo paymentInfo) {
        //修改支付信息
        paymentSerivce.updatePaymentInfo(paymentInfo.getOutTradeNo(),paymentInfo);
        //插入支付日志信息
        PaymentInfoLog paymentInfoLog = new PaymentInfoLog();
        BeanUtils.copyProperties(paymentInfo,paymentInfoLog);
        paymentInfoLog.setPayOrgId(paymentInfo.getPayOrgId());
        paymentLogService.savePaymentInfoLog(paymentInfoLog);
    }
}
