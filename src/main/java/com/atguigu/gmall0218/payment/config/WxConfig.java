package com.atguigu.gmall0218.payment.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:wexinpay.properties")
public class WxConfig {

    /**
     * 微信扫码支付地址
     */
    private static final String WEIXIN_WEB_CODE_ACTION = "/weixin/wxCode";
    public static String weiXinWebNotifyAction;

    // 服务号Id
    public static String appid;
    // 商户号Id

    public static  String partner;
    // 密钥

    public static  String partnerkey;

    @Value("${appid}")
    public   void setAppid(String appid) {
        WxConfig.appid = appid;
    }
    @Value("${partner}")
    public   void setPartner(String partner) {
        WxConfig.partner = partner;
    }
    @Value("${partnerkey}")
    public   void setPartnerkey(String partnerkey) {
        WxConfig.partnerkey = partnerkey;
    }
    @Value("${weiXinWebNotifyAction}")
    public   void setWeixinWebNotifyAction(String weiXinWebNotifyAction) {
        WxConfig.weiXinWebNotifyAction = weiXinWebNotifyAction;
    }

}
