package com.atguigu.gmall0218.payment.module.req;

import lombok.Data;

import java.io.Serializable;

/**
 * <pre>
 *    @author  : liquid
 *    email   : xiaokui.li@guigu.com
 *    time    : 2019/9/169:17 PM
 *    desc    : 支付请求参数实体
 *    version : v1.0
 * </pre>
 */
@Data
public class PaymentReqParam implements Serializable{
    /**
     * 订单号
     */
    private String orderId;
    /**
     * 主体名称
     */
    private String subject;
    /**
     * 终端来源:<br>
     * 1:web<br>
     * 2:wap<br>
     * 3:app<br>
     * 4:微信<br>
     */
    private String requestSource;
    /**
     * 金额
     */
    private Long amount;
    /**
     * 服务器异步通知页面路径
     */
    private String notifyUrl;
    /**
     * 页面跳转同步通知地址
     */
    private String returnUrl;
    /**
     * 支付机构编码
     */
    private String payOrgCode;
}
